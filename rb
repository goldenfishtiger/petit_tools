#!/usr/bin/env ruby
# coding: utf-8

script = []
while(param = ARGV.shift)
	param =~ /^-+h/ and abort(<<USAGE % [$0, $0, $0, $0])
Usage:
  $ ls | %s -n
  $ ls | %s -a 'self[10, 5]'
  $ ls | %s -a 'self[10, 5]' | %s 'gsub(/^/, "echo ")' 
USAGE
	param =~ /^([-+]\d+)/ and diff = $1.to_i and next
	param =~ /^-+n/ and num   = true and next
	param =~ /^-+a/ and array = true and next
	param =~ /^-+l/ and line  = true and next
	param =~ /^-+q/ and quote = true and next
	script << param
end

def puts(l, quote)
	super(quote ? '"%s"' % l : l)
end

if(num)
	n = 0; $stdin.each {|l|
		puts('%6d  %s' % [n += 1, l.chomp], quote)
	}

elsif(array)
	puts($stdin.read.split(/\r?\n/).instance_eval(script.join(' ')), quote)

elsif(line)
	$stdin.each {|l|
		puts(l.chomp.instance_eval(script.join(' ')), quote)
	}

else
	puts($stdin.read.instance_eval(script.join(' ')), quote)

end

__END__

