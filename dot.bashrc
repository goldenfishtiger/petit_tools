# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

alias ls='ls -lrtF --color'
alias cp='cp -p'
alias ex='pushd +1'
alias less='less -X'
alias vi='vim'
alias em='emacs -nw'
alias emacs='emacs --font "VL Gothic-12" --geometry 144x36+192+96'
alias emacs18='emacs --font "VL Gothic-18" --geometry 144x36+192+96'
#alias emacs='emacs --font "Osaka-Mono-12" --geometry 128x40+32'
alias shutdown='echo "It is aliased. Unalias it."'

export EXINIT='set exrc number showmode tabstop=4 ignorecase'
export PAGER='less -X'
export MANPAGER='less -isX'
#export LESSCHARSET='iso8859'
#export LESSCHARSET='utf-8'

#export TMOUT=3600
export TMOUT=0

function uuenc () { gzip --best -c $1 | uuencode uuuuuuuu.gz | sed 's/$/!/g'; }
function uudec () { cat $1 | sed 's/!$//g' | uudecode; gunzip -N uuuuuuuu.gz; rm $1; }
function mkcd () { mkdir "$1"; cd "$1"; }

PS1='\[\e[1;1m\]\n<\u> \d - \t\n\H: $PWD \$ \[\e[0m\]'

