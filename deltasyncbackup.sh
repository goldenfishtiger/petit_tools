#!/bin/bash

BACKUP_TARGET='target1 target2'
#BACKUP_HOST=user@example.com
BACKUP_PATH=backup/targets
#BACKUP_EX=--exclude=id_rsa*

stamp=`date +%Y%m%d_%H%M`
ssh_opt='-o StrictHostKeyChecking=no -o PasswordAuthentication=no'
if [ ${BACKUP_HOST+isDef} ]; then
	coron=:
	ssh_opr="ssh $ssh_opt $BACKUP_HOST"
fi
last_backup=`$ssh_opr ls $BACKUP_PATH | tail -1`
#backup_ex0='--exclude=hyperestraier'
if [ -z "$last_backup" ]; then
	gen=0
else
	gen=$((`$ssh_opr cat $BACKUP_PATH/$last_backup/.gen` + 1))
	link_dest="--link-dest=../$last_backup"
fi
rsync -av --delete $backup_ex0 $BACKUP_EX -e "ssh $ssh_opt" $BACKUP_TARGET $BACKUP_HOST$coron$BACKUP_PATH/$stamp $link_dest
echo $gen > .gen; scp $ssh_opt .gen $BACKUP_HOST$coron$BACKUP_PATH/$stamp

