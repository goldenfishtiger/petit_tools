#!/usr/bin/env ruby
# coding: utf-8

#===============================================================================
#
#	FAT32 を扱う
#
#		http://www.microsoft.com/whdc/system/platform/firmware/fatgen.mspx
#		http://elm-chan.org/docs/fat.html
#
require 'optparse'

begin
	$LOAD_PATH.unshift('/usr/local/lib/ruby')
	require 'libndump'
rescue LoadError
#	raise
end

class String

	def b2i														# リトルエンディアンのバイナリ文字列を数値化
		r = 0; self.reverse.each_byte {|c|
			r <<= 8; r += c
		}
		r
	end
end

class Integer

	def to_h
		sprintf('%d', self).to_s.reverse.scan(/.{1,3}/).join(',').reverse
	end
end

#---------------------------------------------------------------
#
#	FAT32 クラス
#
class Fat32

	def initialize(dev)

		@dev = open(dev); @B = {}

		@B[:S_jmpBoot]		= @dev.read(3)
		@B[:S_OEMName]		= @dev.read(8)
		@B[:PB_BytsPerSec]	= @dev.read(2).b2i
		@B[:PB_SecPerClus]	= @dev.read(1).b2i
		@B[:PB_RsvdSecCnt]	= @dev.read(2).b2i
		@B[:PB_NumFATs]		= @dev.read(1).b2i
		@B[:PB_RootEntCnt]	= @dev.read(2).b2i
		@B[:PB_TotSec16]	= @dev.read(2).b2i
		@B[:PB_Media]		= @dev.read(1)
		@B[:PB_FATSz16]		= @dev.read(2).b2i
		@B[:PB_SecPerTrk]	= @dev.read(2)
		@B[:PB_NumHeads]	= @dev.read(2)
		@B[:PB_HiddSec]		= @dev.read(4)
		@B[:PB_TotSec32]	= @dev.read(4).b2i

		@B[:PB_FATSz32]		= @dev.read(4).b2i
		@B[:PB_ExtFlags]	= @dev.read(2)
		@B[:PB_FSVer]		= @dev.read(2)
		@B[:PB_RootClus]	= @dev.read(4).b2i
		@B[:PB_FSInfo]		= @dev.read(2).b2i
		@B[:PB_BkBootSec]	= @dev.read(2)
		@B[:PB_Reserved]	= @dev.read(12)
		@B[:S_DrvNum]		= @dev.read(1)
		@B[:S_Reserved1]	= @dev.read(1)
		@B[:S_BootSig]		= @dev.read(1)
		@B[:S_VolID]		= @dev.read(4)
		@B[:S_VolLab]		= @dev.read(11)
		@B[:S_FilSysType]	= @dev.read(8)

#		@B.each {|k, v| puts('B%-20s: %s' % [k, v.inspect]) }

		@RootDirSectors = ((@B[:PB_RootEntCnt] * 32) + (@B[:PB_BytsPerSec] - 1)) / @B[:PB_BytsPerSec]
		@FATSz = @B[:PB_FATSz16] != 0 ? @B[:PB_FATSz16] : @B[:PB_FATSz32]
		@TotSec = @B[:PB_TotSec16] != 0 ? @B[:PB_TotSec16] : @B[:PB_TotSec32]
		@DataSec = @TotSec - (@B[:PB_RsvdSecCnt] + (@B[:PB_NumFATs] * @FATSz) + @RootDirSectors)
		@CountofClusters = @DataSec / @B[:PB_SecPerClus]
		if(@CountofClusters < 4085)
			raise('Volume is FAT12')
		elsif(@CountofClusters < 65525)
			raise('Volume is FAT16')
		end
		@FirstDataSector = @B[:PB_RsvdSecCnt] + (@B[:PB_NumFATs] * @FATSz) + @RootDirSectors

		@FSI_ = {}
		@dev.seek(@B[:PB_FSInfo] * @B[:PB_BytsPerSec])
		@FSI_[:LeadSig]		= @dev.read(4)
		@FSI_[:Reserved1]	= @dev.read(480)
		@FSI_[:StrucSig]	= @dev.read(4)
		@FSI_[:Free_Count]	= @dev.read(4).b2i
		@FSI_[:Nxt_Free]	= @dev.read(4).b2i
		@FSI_[:Reserved2]	= @dev.read(12)

#		@FSI_.each {|k, v| puts('FSI_%-20s: %s' % [k, v.inspect]) }
	end

	def root_cluster
		@B[:PB_RootClus]
	end

	def each_fatc(n)
		while(n < 0x0FFFFFF8)
			yield(n)
			@dev.seek(@B[:PB_RsvdSecCnt] * @B[:PB_BytsPerSec] + n * 4)
			n = @dev.read(4).b2i & 0x0FFFFFFF
		end if(n != 0)
	end

	def c2o(n)													# クラスタ番号をオフセット位置に変換
		(((n - 2) * @B[:PB_SecPerClus]) + @FirstDataSector) * @B[:PB_BytsPerSec]
	end

	def entries(entry)
		bin = ''; each_fatc(entry[:FstClus]) {|n|
			@dev.seek(c2o(n))
			bin << @dev.read(@B[:PB_SecPerClus] * @B[:PB_BytsPerSec])
		}
		parse_entries(bin)
	end

	def each_cluster(entry)
		size = entry[:FileSize]
		csize = @B[:PB_SecPerClus] * @B[:PB_BytsPerSec]
		each_fatc(entry[:FstClus]) {|n|
			@dev.seek(c2o(n))
			yield(@dev.read(size > csize ? csize : size)); size -= csize
		}
	end

	def parse_entries(bin)
		lfn = ''
		entries = []
		999.times {|n|

			_DIR = Fat32entry.new

			_DIR[:Attr] = bin[n * 32 + 11].b2i

			# LFN(ロングファイルネームエントリ)対応
			_DIR[:Attr] == 0x0F and lfn = bin[n * 32 + 1, 10] + bin[n * 32 + 14, 12] + bin[n * 32 + 28, 4] + lfn and next

			# ディレクトリ/ファイル以外無視
			_DIR[:Attr] & 0b11001110 == 0 or next

			# 無効エントリチェック
			_DIR[:Name0] = bin[n * 32].b2i
			# FREE check
			_DIR[:Name0] == 0xE5 and next
			# LAST check
			_DIR[:Name0] == 0x00 and break

			_DIR[:Name] = bin[n * 32, 11]
			_DIR[:LfName] = lfn.size > 0 ? lfn.unpack('v*').pack('C*').gsub(/\x00.*/, '') : nil; lfn = ''
#			_DIR[:NTRes]
#			_DIR[:CrtTimeTenth]
#			_DIR[:CrtTime]
#			_DIR[:CrtDate]
#			_DIR[:LstAccDate]
			_DIR[:FstClus] = (bin[n * 32 + 20, 2].b2i << 16) + bin[n * 32 + 26, 2].b2i
			its = []
			_DIR[:WrtTime] = it = bin[n * 32 + 22, 4].b2i
			its.unshift(it & 0x1F * 2)
			its.unshift((it >> 5) & 0x3F)
			its.unshift((it >> 11) & 0x1F)
			_DIR[:WrtDate] = it = bin[n * 32 + 24, 4].b2i
			its.unshift(it & 0x1F)
			its.unshift((it >> 5) & 0x0F)
			its.unshift(((it >> 9) & 0x3F) + 1980)
			_DIR[:WrtTimeI] = Time.local(*its) rescue _DIR[:WrtTimeI] = Time.at(0)
			_DIR[:FileSize] = bin[n * 32 + 28, 4].b2i

			entries << _DIR
		}
		entries
	end

	def find(target = [], entry)
		target.size == 0 and return(entry)
		entries(entry).each {|entry|
			(entry[:LfName] == target.first or entry[:Name].strip == target.first.sub(/\./, '')) and return(find(target[1, 99], entry))
		}
		raise('No such file or directory')
	end

	def print_free
		puts('%d Clusters free' % @FSI_[:Free_Count])
	end

	def print_path(target)
		puts('Path name "0:%s/"' % target.join('/'))
	end
end

class Fat32entry < Hash

	@@printers = {}

	def directory?
		self[:Attr] & 0x10 != 0
	end

	def file?
		self[:Attr] & 0x20 != 0
	end

	def print(style = :UNIX)
		puts(@@printers[style].call(self))
	end

	def self.bind_printer(type)
		@@printers[type] = Proc.new
	end
end

Fat32entry.bind_printer(:UNIX) {|entry|
	'%srwxr-xr-x %10d %18s %s' %
		[entry.directory? ? 'd' : '-', entry[:FileSize], entry[:WrtTimeI].strftime('%Y/%m/%d %a %H:%M'), entry[:LfName] || entry[:Name]]
}

Fat32entry.bind_printer(:X1) {|entry|
	'%3s*      "0:%-13s.%3s"         \'%18s %13s' % 
		[entry.directory? ? 'Dir' : 'Bin', entry[:Name][0, 8], entry[:Name][8, 3], entry[:WrtTimeI].strftime('%y/%m/%d %a %H:%M').upcase, entry[:FileSize].to_h]
}

Fat32entry.bind_printer(:PC) {|entry|
	'%8s%4s%14s%17s' % [entry[:Name][0, 8], entry[:Name][8, 3], entry[:FileSize].to_h, entry[:WrtTimeI].strftime('%y-%m-%d   %H:%M')]
}

__END__

