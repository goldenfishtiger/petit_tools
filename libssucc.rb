#!/usr/bin/env ruby

=begin

* ssucc.rb

	ファイル補完に適する、プリフィクスを採番するライブラリ

	k = ''; 40.times { print(k.ssucc!, ', ') }
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, z0, z1, z2, z3, 

=end

class String

	@@ssucc_keys = ''
#	('0'..'9').each {|k| @@ssucc_keys << k }
	('a'..'z').each {|k| @@ssucc_keys << k }
	@@ssucc_k0 = @@ssucc_keys[0]
	@@ssucc_k9 = @@ssucc_keys[-1, 1]

	def ssucc!									# sortable succ
		self.replace(self.size != 0 ? (@@ssucc_keys =~ /#{self[-1, 1]}(.)/ ? @@ssucc_k9 * (self.size - 1) + $1 : @@ssucc_k9 * self.size + @@ssucc_k0) : @@ssucc_k0)
	end
end

__END__

