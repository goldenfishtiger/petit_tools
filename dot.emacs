;; .emacs

;;; uncomment this line to disable loading of "default.el" at startup
(setq inhibit-default-init t)

;; enable visual feedback on selections
(setq transient-mark-mode nil)

;; default to better frame titles
(setq frame-title-format
      (concat  "%b - emacs@" (system-name)))

;; default to unified diffs
(setq diff-switches "-u")

;; always end a file with a newline
;(setq require-final-newline 'query)

;;; uncomment for CJK utf-8 support for non-Asian users
;; (require 'un-define)

(global-set-key "\C-h" 'delete-backward-char)
(global-set-key "\C-z" 'scroll-down)
(global-set-key "\C-u" 'undo)
(global-set-key "\C-]" 'call-last-kbd-macro)

(setq inhibit-startup-message t) 
(menu-bar-mode -1)
(tool-bar-mode -1)
(column-number-mode t)
(setq blink-matching-paren nil)
 
(setq default-tab-width 4)
(setq default-fill-column 64)
(setq text-mode-hook 'turn-on-auto-fill)
 
;(setq make-backup-files nil)
(setq auto-save-default nil)
 
;; ATOK X3 for Linux
;(setq iiimcf-server-control-hostlist '("unix:/tmp/.iiim-mitsu/:0"))
;(require 'iiimcf-sc)
;(setq iiimcf-server-control-default-language "ja")
;(setq iiimcf-server-control-default-input-method "atokx3")
;(setq default-input-method 'iiim-server-control)
;(define-key global-map "\C-j" (lambda ()
;    (interactive)
;    (if current-input-method (inactivate-input-method))
;    (toggle-input-method)))
;(define-key global-map "\C-o" (lambda ()
;    (interactive)
;    (inactivate-input-method)))
;(global-set-key [?\S- ] 'iiimcf-server-control-keyforward)
;(setq iiimcf-UI-input-method-title-format "<ATOK:%s>")
;(setq iiimcf-UI-preedit-use-face-p "window-system")
;;(iiimp-debug)

;; mozc
(require 'mozc)
(set-language-environment "Japanese")
(prefer-coding-system 'utf-8)
(setq default-input-method "japanese-mozc")
(setq mozc-candidate-style 'echo-area) 
(define-key global-map "\C-j" (lambda ()
    (interactive)
    (if current-input-method (inactivate-input-method))
    (toggle-input-method)))
(define-key global-map "\C-o" (lambda ()
    (interactive)
    (inactivate-input-method)))

(set-cursor-color "#ffffff")
(set-foreground-color "white")
(set-background-color "darkblue")
 
(defvar keep-screen-column nil)
 
(defun screen-column ()
	"get screen column"
	(save-excursion
		(let ((s-column (current-column)))
			(vertical-motion 0)
			(- s-column (current-column)))))
 
(defun move-beginning-of-screen-line ()
	"move beginning of screen line"
	(interactive)
	(vertical-motion 0))
 
(defun move-end-of-screen-line ()
	"move end of screen line"
	(interactive)
	(vertical-motion 1)
	(backward-char 1))
 
(defun next-screen-line ()
	"next screen line"
	(interactive)
	(if truncate-lines
		(next-line 1)
		(if (not (eq last-command 'next-screen-line))
			(setq keep-screen-column (screen-column)))
		(vertical-motion 1)
		(move-to-column (+ (current-column) keep-screen-column))))
 
(defun previous-screen-line ()
	"previous screen line"
	(interactive)
	(if truncate-lines
		(previous-line 1)
		(if (not (eq last-command 'next-screen-line))
			(setq keep-screen-column (screen-column)))
		(vertical-motion -1)
		(move-to-column (+ (current-column) keep-screen-column)))
	(setq this-command 'next-screen-line))
 
(global-set-key "\C-a" 'move-beginning-of-screen-line)
(global-set-key "\C-e" 'move-end-of-screen-line)
(global-set-key "\C-n" 'next-screen-line)
(global-set-key "\C-p" 'previous-screen-line)
 
(set-scroll-bar-mode nil)
 
(require 'wb-line-number)
(setq truncate-partial-width-windows nil)
(wb-line-number-toggle)
;(if (version<= "26.0.50" emacs-version)
;	(global-display-line-numbers-mode))

(setq-default line-spacing 0.2)
(setq tab-stop-list
	'(2 4 6 8 10 12 14 16 18 20 22 24))
;	'(4 8 12 16 20 24))

(setq tab-width 4)
(add-hook 'text-mode-hook '(lambda()
	(setq tab-width 4)
	(setq tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68 72 76 80))
	(setq indent-tabs-mode nil)
))

