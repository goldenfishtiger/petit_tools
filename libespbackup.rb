# coding: utf-8

#===============================================================
#
#	Evenly Spaced Backup
#
#===============================================================

class EspBackup

	def initialize(sgens = 3, lgens = sgens)
		@sgens = sgens; @lgens = lgens
		@bhs = []				# 3: [ '1110', '1100', '1000', '0100', '0110', '0111', '0000' ]
		sgens.times {|i0|
			i = sgens - i0		# sgens..1
			@bhs << ('1' * i + '0' * (i0 + 1))
		}
		sgens.times {|i0|
			i = i0 + 1			# 1..sgens
			@bhs << ('0' + '1' * i + '0' * (sgens - i))
		}
		@bhs << '0' * (sgens + 1)
	end

	def stored=(gen)
		@rgens = {}
		@lgens.times {
			@rgens[gen] = true
			(gen -= 1) < 0 and return
		}
		bw = ('%b' % gen).size
		@bhs.each {|bh|
			(it = (bh + '0' * bw)[0, bw].to_i(2)) <= gen and @rgens[it] = true
			@rgens.size == @sgens + @lgens and return
		}
	end

	def rgens
		@rgens.keys.sort
	end

	def should_delete?(gen)
		!@rgens[gen]
	end
end

__END__

#---------------------------------------------------------------
#
#	テスト
#
backups = {}
ebackup = EspBackup.new				# (3, 3) まばら x 3、連続 x 3
20.times {|gen|
	backups[gen] = true				# gen 世代のバックアップを実行
	ebackup.stored = gen			# 最新バックアップの世代を申告
	backups.delete_if {|g, v|
		ebackup.should_delete?(g)	# g 世代のバックアップを消すべきか確認
	}
	puts('%d:' % gen)
	backups.each {|g, v|
		puts(' %d' % g)
	}
	puts
}

#---------------------------------------------------------------
#
#	利用サンプル
#
backups = {}; last_gen = -1
Dir.glob('xxx_backups/*').each {|path|
	gen = open('%s/.gen' % path).read.to_i
	backups[gen] = path
	gen > last_gen and last_gen = gen
}
ebackup = EspBackup.new
ebackup.stored = last_gen
backups.each {|gen, path|
	ebackup.should_delete?(gen) and p('rm -rf %s' % path)
}

#---------------------------------------------------------------
#
#	図化
#
require 'bundler/setup'
require './TrueLegacyGraphicsCairo'

win  = LegacyGraphics.new(nil, nil, nil, nil, 640, 640)

mx = my = 20
300.times {|gen|
	ebackup = EspBackup.new
	puts('%d:' % gen)
	ebackup.stored = gen
	ebackup.rgens.each {|rgen|
		puts(' %d' % rgen)
		win.pset(rgen * 2 + mx, gen * 2 + my, 7)
	}	
	puts
}

win.close

__END__

